=== Plugin Name ===
Contributors: rianrainey
Tags: gravity forms, html5, placeholder
Requires at least: 3.0.1
Tested up to: 3.4
License: GPLv2 or later
Stable tag: trunk
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds a placeholder input field to Gravity Forms that is displayed as HTML5 placeholder text.

== Description ==

Adds a placeholder input field to Gravity Forms that is displayed as HTML5 placeholder text. Special thanks to Jorge Pedret. Other Gravity Form plugins use the label field without any other option. This plugin provides a new placeholder text input box that renders HTML5 placeholder text.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `bw_gravityform_placeholder.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==