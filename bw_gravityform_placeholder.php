<?php
/**
 * Plugin Name: Gravity Forms Placeholder
 * Description: Adds a placeholder input field to Gravity Forms that is displayed as HTML5 placeholder text. Special thanks to Jorge Pedret.
 * Version: 1.0
 * Author: Built Well Labs
 * @see http://jorgepedret.com/web-development/placeholder-text-for-gravity-form-input-elements/
 * Author URI: http://www.builtwelllabs.com
 */

/* Add a custom field to the field editor (See editor screenshot) */
add_action("gform_field_standard_settings", "add_gf_placeholder_input_field", 10, 2);
function add_gf_placeholder_input_field($position, $form_id){
	// Create settings on position 25 (right after Field Label)
	if($position == 25){
		?>
		<li class="admin_label_setting field_setting" style="display: list-item; ">
			<label for="field_placeholder">Placeholder Text
				<!-- Tooltip to help users understand what this field does -->
				<a href="javascript:void(0);" class="tooltip tooltip_form_field_placeholder" tooltip="&lt;h6&gt;Placeholder&lt;/h6&gt;Enter the placeholder/default text for this field.">(?)</a>
			</label>
			<input type="text" id="field_placeholder" class="fieldwidth-3" size="35" onkeyup="SetFieldProperty('placeholder', this.value);">
		</li>
		<?php
	}
}

/* Now we execute some javascript technicalitites for the field to load correctly */
add_action("gform_editor_js", "gfp_add_placeholder_text");
function gfp_add_placeholder_text(){
	?>
	<script>
		//binding to the load field settings event to initialize the checkbox
		jQuery(document).bind("gform_load_field_settings", function(event, field, form){
			jQuery("#field_placeholder").val(field["placeholder"]);
		});
	</script>
	<?php
}

/* We use jQuery to read the placeholder value and inject it to its field */
add_action('gform_enqueue_scripts',"gfp_enque_scripts", 10, 2);
function gfp_enque_scripts($form, $is_ajax=false){
	?>

		<?php
		/* Go through each one of the form fields */
		foreach($form['fields'] as $i=>$field){
			/* Check if the field has an assigned placeholder */
			if(isset($field['placeholder']) && !empty($field['placeholder'])){
				/* If a placeholder text exists, inject it as a new property to the field using jQuery */
				?>
				<script>
				jQuery(function(){
					jQuery('#input_<?php echo $form['id']?>_<?php echo $field['id']?>').attr('placeholder','<?php echo $field['placeholder']?>');
				});
				</script>
				<?php
			}
		}
		?>

	<?php
}
?>